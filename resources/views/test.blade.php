@extends('layout')

@section('content')

<div class="row">
	 
	@section('header')
		<h1>Welcome to my website</h1>
		<p>We are creating something beautiful today.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
	@stop

	<h2>Sign In</h2>
	@if(Auth::id() != null)
			<p><a href="{{ URL::to('logout') }}">Logout</a></p>
			<p>{{Auth::id()}}</p>
			<p>{{Auth::user()->email}}</p>
	@endif

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	{!! Html::ul($errors->all(), array('class'=>'errors')) !!}

	{!! Form::open(array('url' => 'login','class'=>'form-inline')) !!}

	<div class="form-group">
		{!! Form::label('email', 'E-Mail Address', ['class' => 'control-label']) !!}<br/>
		{!! Form::text('email', 'example@gmail .com', array('class' => 'form-control')) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password', 'Password', ['class' => 'control-label']) !!}<br/>
		{!! Form::password('password', array('class' => 'form-control')) !!}
	</div>
	<br/><br/>
	{!! Form::submit('Sign In' , array('class' => 'btn btn-primary')) !!}

	{!! Form::close() !!}
</div>

@stop