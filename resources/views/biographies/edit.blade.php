@extends('layout')

@section('header')
	<h1>Updating "{{ $biography->name }}"</h1>
	<p class="lead">Edit and save this info below, or <a href="{{ route('biographies.index') }}">go back to records.</a></p>
@stop

@section('content')
	
	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	{!! Form::model($biography, [
	    'method' => 'PATCH',
	    'route' => ['biographies.update', $biography->id]
	]) !!}

	<div class="form-group">
	    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
	    {!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('ic_num', 'My-Kad Number:', ['class' => 'control-label']) !!}
	    {!! Form::text('ic_num', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('phone', 'Phone Number:', ['class' => 'control-label']) !!}
	    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('gender', 'Gender:', ['class' => 'control-label']) !!}
		{!! Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), null, array('class'=>'form-control')) !!}
	</div>

	{!! Form::submit('Update Biography', ['class' => 'btn btn-primary']) !!}

	{!! Form::close() !!}

@stop