@extends('layout')

@section('header')
	<h1>{{ $biography->name }}</h1>
	<p class="lead">Read the details below, or <a href="{{ route('biographies.index') }}">go back to records.</a></p>
@stop

@section('content')
	<h3>{{ $biography->phone}}</h3>
	<h3>{{ $biography->gender}}</h3>
	
	{!! Form::open([
	    'method' => 'DELETE',
	    'route' => ['biographies.destroy', $biography->id]
	]) !!}
	    {!! Form::submit('Delete this Biography?', ['class' => 'btn btn-danger']) !!}
	{!! Form::close() !!}
@stop