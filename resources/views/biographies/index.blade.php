@extends('layout')

@section('header')
	<h1>Biography Records</h1>
@stop

@section('content')
	
	@if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

	@foreach($biographies as $bio)
	    <h3>{{ $bio->name }}</h3>
	    <p>{{ $bio->ic_num}}</p>
	    <p>
	        <a href="{{ route('biographies.show', $bio->id) }}" class="btn btn-info">Details</a>
	        <a href="{{ route('biographies.edit', $bio->id) }}" class="btn btn-primary">Update</a>
	    </p>
	    <hr>
	@endforeach
@stop