@extends('layout')

@section('header')
	<h1>Create a Biography</h1>
@stop

@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	{!! Form::open(array('url' => 'biographies')) !!}
		<div class="form-group">
		    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('ic_num', 'My-Kad Number:', ['class' => 'control-label']) !!}
		    {!! Form::text('ic_num', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('phone', 'Phone Number:', ['class' => 'control-label']) !!}
		    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
		    {!! Form::label('gender', 'Gender:', ['class' => 'control-label']) !!}
			{!! Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), null, array('class'=>'form-control')) !!}
		</div>

		{!! Form::submit('Create New Biography', ['class' => 'btn btn-primary']) !!}
   
    {!! Form::close() !!}
@stop