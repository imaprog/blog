<!DOCTYPE html>
<html>
    <head>
        <title>Laravel is awesome</title>

        {!! Html::style('css/app.css') !!}

        {!! Html::script('js/jquery.min.js') !!}
        {!! Html::script('js/bootstrap.min.js') !!}

        <style>
        body { padding-top: 60px; }
        @media (max-width: 979px) {
            body { padding-top: 0px; }
        }
        </style>
    </head>
<div class="container">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="row col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Blog</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>{!! HTML::link('/biographies/create', 'Create', true) !!}</li>
                    <li>{!! HTML::link('/biographies', 'Biography', true) !!}</li>
                    <li><a href="">Search</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class="jumbotron">
        @yield('header')
    </div>
    <body>
    <div class="container">
        <div class="row col-md-4">
            @yield('content')
        </div>
        <div class="row col-md-4" align="center">
            <h2>Laravel 5</h2>
            {!! HTML::image('img/l5.png', 'laravel', array( 'width' => 300, 'height' => 200 )) !!}
            <h3>Get to know the MVC Pattern of Laravel</h3>
        </div>
        <div class="row col-md-4" align="center">
            <h2>MVC Pattern</h2>
            {!! HTML::image('img/mvc.png', 'mvc', array( 'width' => 300, 'height' => 200 )) !!}
        </div>
    </div>
    </body>    
</div>
</html>