<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biography extends Model
{
	//permitted parameters
    protected $fillable = [
        'name',
        'ic_num',
        'phone',
        'gender'
    ];
}
