<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('test');
});

Route::get('biography', [
	'as' => 'biography',
	'uses' => 'BiographyController@getBiography'
]);

Route::resource('biographies', 'BiographyController');

// route to process the form
Route::post('login', array('uses' => 'BiographyController@doLogin'));

Route::get('logout', array('uses' => 'BiographyController@doLogout'));