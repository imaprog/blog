<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Biography;
use Session;
use Auth;

class BiographyController extends Controller
{

    public function doLogin()
    {
    // validate the info, create rules for the inputs
    $rules = array(
        'email'    => 'required|email', // make sure the email is an actual email
        'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('/')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        // create our user data for the authentication
        $userdata = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            // validation successful!
            // redirect them to the secure section or whatever
            // return Redirect::to('secure');
            // for now we'll just echo success (even though echoing in a controller is bad)
            Session::flash('flash_message', 'Login successfully!');

            return Redirect::to('/');

        } else {        

            // validation not successful, send back to form 
            return Redirect::to('/');

        }

    }
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application

        Session::flash('flash_message', 'Logout successfully!');

        return Redirect::to('/'); // redirect the user to the login screen
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biographies = Biography::all();

        return view('biographies.index')->withBiographies($biographies);
    }

    public function getBiography()
    {
        $page_title = "is a Test";
        $status = false;
        return view('biographies.try')
               ->with("page_title", $page_title)
               ->with("status", $status);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('biographies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation scripts
        $this->validate($request, [
            'name' => 'required',
            'ic_num' => 'required',
            'phone' => 'required',
            'gender' => 'required'
        ]);

        //store post data to DB
        $input = $request->all();

        Biography::create($input);

        Session::flash('flash_message', 'Biography successfully Created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $biography = Biography::findOrFail($id);

        return view('biographies.show')->withBiography($biography);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $biography = Biography::findOrFail($id);

        return view('biographies.edit')->withBiography($biography);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $biography = Biography::findOrFail($id);

        //validation scripts
        $this->validate($request, [
            'name' => 'required',
            'ic_num' => 'required',
            'phone' => 'required',
            'gender' => 'required'
        ]);

        $input = $request->all();

        $biography->fill($input)->save();

        Session::flash('flash_message', 'Biography successfully added!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $biography = Biography::findOrFail($id);

        $biography->delete();

        Session::flash('flash_message', 'Biography successfully deleted!');

        return redirect()->route('biographies.index');
    }
}
